#include "MyLexer.hh"

namespace prs {
	MyLexer::MyLexer(std::istream & yyin, std::ostream & yyout, location & loc) :
		yyFlexLexer(yyin, yyout),
		loc(loc) {
	}
}
