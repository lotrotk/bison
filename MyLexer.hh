#pragma once

#if !defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#include "location.hh"
#include "MyParser.hh"

namespace prs {
	class MyLexer : public yyFlexLexer {
	public:
		MyLexer(std::istream & yyin, std::ostream & yyout, location & loc);
		MyParser::symbol_type myyylex();
	private:
		location & loc;
	};
}
