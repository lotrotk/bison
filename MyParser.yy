%language "c++"
%defines "MyParser.hh"
%output "MyParser.cc"

%define api.value.type variant
%define api.token.constructor
%define api.namespace {prs}
%define parser_class_name {MyParser}
%locations

%{
	#include <cmath>

	#include "MyLexer.hh"	

	#undef yylex
	#define yylex lexer.myyylex
%}

%code requires {
	#include <ostream>
	#include <iostream>//for debugging only

	#include "location.hh"
	#include "symrec.hh"

	namespace prs {
		class MyLexer;
	}
}

%parse-param {std::ostream & os}
%parse-param {location & loc}
%parse-param {prs::MyLexer & lexer}

%token END 0 "end of line"
%token <double> NUM
%token <symrec *> VAR FNCT
%type <double> exp
%token EQ
%token MIN
%token PLUS
%token STAR
%token FWSLASH
%token POW
%token LPAREN
%token RPAREN
%token NEWL

%precedence '='
%left MI PLUS
%left STAR FWSL
%precedence NEGATIVE
%right HAT

%%

input:
	%empty
|	input line
;

line:
	NEWL
|	exp NEWL				{os << $1 << std::endl;}
|	error NEWL				{yyerrok;}
;

exp:
	NUM						{$$ = $1;}
|	VAR						{$$= $1->value.var;}
|	VAR EQ exp				{$$ = $3; $1->value.var = $3;}
|	exp PLUS exp			{
								std::cerr << "executing " << $1 << "+" << $3 << std::endl;
								$$ = $1 + $3;
							}
|	exp MIN exp				{$$ = $1 - $3;}
|	exp STAR exp			{$$ = $1 * $3;}
|	exp FWSL exp			{$$ = $1 / $3;}
|	exp POW exp				{
								std::cerr << "executing " << $1 << "^" << $3 << std::endl;
								$$ = pow($1, $3);
							}
|	MIN exp %prec NEGATIVE	{$$ = -$2;}
|	LPAREN exp RPAREN		{$$ = $2;}
|	FNCT LPAREN exp RPAREN	{
								std::cerr << "executing " << $1->name << "(" << $3 << ")" << std::endl;
								$$ = (*($1->value.fnctptr))($3);
							}
;
%%

