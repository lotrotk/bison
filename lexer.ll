%{
	#include "MyLexer.hh"
	#define YY_DECL prs::MyParser::symbol_type prs::MyLexer::myyylex()

	#define UPDATE_MY_LOC() loc += yyleng;
%}

%option c++
%option outfile="lexer.cc"
%option yyclass="prs::MyLexer"
%option noyywrap

LPAREN	"("
RPAREN	")"
PLUS	"+"
MIN		"-"
STAR	"*"
FWSL	"/"
POW		"^"
EQ		"="
DIGIT   [0-9]
ID      [a-z][a-z0-9]*
WS		[ \r\t]
NEWL	"\n"

%%

%{
    loc.step();
%}

{WS}		{
				loc += 1;
				continue;
			}
{NEWL}		{
				loc.lines();
				return MyParser::make_NEWL(loc);
			}
<<EOF>>		{return MyParser::make_END(loc);}

{DIGIT}+	{
				UPDATE_MY_LOC();
				return MyParser::make_NUM(atoi(yytext), loc);
			}
{DIGIT}+"."{DIGIT}* {
				UPDATE_MY_LOC();
				return MyParser::make_NUM(strtod(yytext, nullptr), loc);
			}
{ID}		{
				std::string name{yytext};
				symrec const * rec = getsym(name);
				if(!rec) {
					rec = putsym(std::move(name), symrec::Type::var);
				}
				UPDATE_MY_LOC();
				if(rec->type == symrec::Type::var) {
					return MyParser::make_VAR(rec, loc);
				}
				return MyParser::make_FNCT(rec, loc);
			}
{EQ}		{
				UPDATE_MY_LOC();
				return MyParser::make_EQ(loc);
			}
{STAR}		{
				UPDATE_MY_LOC();
				return MyParser::make_STAR(loc);
			}
{FWSL}		{
				UPDATE_MY_LOC();
				return MyParser::make_FWSL(loc);
			}
{PLUS}		{
				UPDATE_MY_LOC();
				return MyParser::make_PLUS(loc);
			}
{POW}		{
				UPDATE_MY_LOC();
				return MyParser::make_POW(loc);
			}
{MIN}		{
				UPDATE_MY_LOC();
				return MyParser::make_MIN(loc);
			}
{LPAREN}	{
				UPDATE_MY_LOC();
				return MyParser::make_LPAREN(loc);
			}
{RPAREN}	{
				UPDATE_MY_LOC();
				return MyParser::make_RPAREN(loc);
			}

%%
