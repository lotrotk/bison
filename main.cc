#include "MyLexer.hh"
#include "MyParser.hh"
#include "symrec.hh"

#include <iostream>

using namespace prs;

namespace prs {
	void MyParser::error(location const & loc, std::string const & msg) {
		std::cerr << loc << ": " << msg << std::endl;
	}
} //namespace prs

int main(int const argc, char const * argv[]) {
	init_table();
	location loc;
	MyLexer lexer(std::cin, std::cout, loc);
	MyParser parser(std::cout, loc, lexer);
	return parser.parse();
}
