#include "symrec.hh"

#include <cmath>
#include <utility>

struct init{
	char const * fname;
	double (*fnct)(double);
};
typedef struct init init;

static init const arith_fncts[] = {
	{"atan", atan},
	{"cos", cos},
	{"exp", exp},
	{"ln", log},
	{"sin", sin},
	{"sqrt", sqrt},
	{0, 0},
};


void init_table() {
	int i;

	for(i = 0; arith_fncts[i].fname; ++i) {
		init const * const fnc = arith_fncts + i;
		symrec * ptr = putsym(fnc->fname, symrec::Type::func);
		ptr->value.fnctptr = fnc->fnct;
	}
}

namespace {
	std::unique_ptr<symrec> sym_table;
} //namespace

symrec * putsym(std::string && sym_name, symrec::Type const sym_type) {
	std::unique_ptr<symrec> ptr = std::make_unique<symrec>();

	ptr->name = std::move(sym_name);
	ptr->type = sym_type;
	ptr->value.var = 0.;
	ptr->value.fnctptr = nullptr;
	ptr->next = std::move(sym_table);
	sym_table = std::move(ptr);
	return sym_table.get();
}

symrec * getsym(std::string const & sym_name) {
	symrec * ptr = nullptr;

	for(ptr = sym_table.get(); ptr != (symrec *)0; ptr = ptr->next.get()) {
		if(ptr->name == sym_name) {
			return ptr;
		}
	}
	return nullptr;
}
