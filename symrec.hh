#pragma once

#include <memory>
#include <string>

typedef double (*func_t)(double);

struct symrec
{
	enum struct Type {
		var,
		func,
	};

	std::string name;
	Type type;
	union {
		double var;
		func_t fnctptr;
	} value;
	std::unique_ptr<symrec> next;
};

void init_table();

symrec * putsym(std::string &&, symrec::Type);
symrec * getsym(std::string const &);
